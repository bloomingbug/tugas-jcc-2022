<?php
require_once ('animal.php');

class Ape extends Animal {
    public $legs = 2;

    public function yell(){
        $this -> get_name();
        $this -> get_legs();
        $this -> get_cold_blooded();
        echo "Yell : Auooo <br><br>";
    }
};
?>