<?php
require_once ('animal.php');
require_once ('Frog.php');
require_once ('Ape.php');

$sheep = new Animal("shaun");

// echo "Name : " . $sheep->name . "<br>"; // "shaun"
// echo "legs : " . $sheep->legs . "<br>"; // 4
// echo "blooded : " . $sheep->cold_blooded . "<br>"; // "no"
$sheep -> get_name();
$sheep -> get_legs();
$sheep -> get_cold_blooded();
echo "<br>";
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

// index.php
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
$kodok->jump(); // "hop hop"
?>