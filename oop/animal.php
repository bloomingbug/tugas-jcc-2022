<?php
class Animal {
    public $name;
    public $legs = 4;
    public $cold_blooded = 'no';
    
    public function __construct($nama){
        $this -> name = $nama;
    }

    public function get_name(){
        echo "Name : " . $this -> name . "<br>";
    }

    public function get_legs(){
        echo "legs : " . $this -> legs . "<br>";
    }

    public function get_cold_blooded(){
        echo "cold blooded : " . $this -> cold_blooded . "<br>";
    }
}

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>