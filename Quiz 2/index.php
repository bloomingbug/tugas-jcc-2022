<?php
function pagar_bintang($integer){
    $str = ""; 
    for($i = 1; $i <= $integer; $i++){
        if ($i % 2 == 0) {
            for($j = 1; $j <= $integer; $j++){
                $str .= "* ";
            };
            $str . "<br>";
        } else {
            for($j = 1; $j <= $integer; $j++){
                $str .= "# ";
            };
        }
        $str .= "<br>";
    };
    return $str;
};

// pagar_bintang(5);
echo pagar_bintang(5);
echo pagar_bintang(8);
echo pagar_bintang(10);


// B

function xo($str) {
 $o = substr_count($str, "o");
 $x = substr_count($str, "x");
 if ($o === $x) {
     return "Benar<br>";
 } else {
     return "Salah<br>";
 }

}

// Test Cases
echo xo('xoxoxo'); // "Benar"
echo xo('oxooxo'); // "Salah"
echo xo('oxx'); // "Salah"
echo xo('xxooox'); // "Benar"
echo xo('xoxooxxo'); // "Benar"

?>